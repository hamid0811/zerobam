<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/user/show', 'Api\UserController@show')->middleware('auth:api');

Route::get('/trending', 'Api\TrendingController@index');

Route::post('/login', 'Api\Auth\LoginController@login');
Route::post('/register', 'Api\Auth\RegisterController@register');

Route::post('/playlist/sync', 'Api\PlaylistController@sync')->middleware('auth:api');

Route::get('/album/{id}', 'Api\AlbumController@show');
Route::get('/search/artist/{query}', 'ArtistController@search');

Route::group(['prefix' => 'artist'], function(){
    Route::get('/{id}', 'Api\ArtistController@show');
});

Route::group(['prefix' => 'track', 'middleware' => 'auth:api'], function(){
    Route::patch('/{id}', 'Api\TrackController@update');
    Route::get('/syncs/{id}', 'Api\LyricSyncsController@show');
    Route::get('/lyric/{id}', 'Api\LyricController@show');
    Route::put('/lyrics/{id}', 'Api\TrackController@store');
    Route::get('/trending', 'Api\TrendingController@index');
    Route::post('/save-track', 'Api\SavedTrackController@store');
    Route::get('/is-saved/{id}', 'Api\SavedTrackController@show');
    Route::delete('/remove-saved/{id}', 'Api\SavedTrackController@destroy');
    Route::post('/saved-tracks', 'Api\SavedTrackController@index');
});

Route::get('/playlists', 'Api\PlaylistController@index')->middleware('auth:api');

Route::group(['prefix' => 'playlist', 'middleware' => 'auth:api'], function(){
    Route::post('/create', 'Api\PlaylistController@store');
    Route::get('/{id}', 'Api\PlaylistController@show');
    Route::post('/add-track', 'Api\PlaylistController@add_track_to_playlist');
});
