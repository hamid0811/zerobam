<?php

use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/check_user_type', function(){
    if(Auth::user()->isAdmin()){
        return redirect('/admin');
    }else{
        return redirect('/user');
    }
});

Auth::routes();

Route::group(['prefix' => 'admin/crawlers/',  'middleware' => 'admin'], function() {
    Route::get('/radiojavan/', 'crawlers\RadioJavanController@index');
    Route::get('/radiojavan/singles/{name}', 'crawlers\RadioJavanController@singles');
    Route::post('/radiojavan/singles/store', 'crawlers\RadioJavanController@saveToDatabase');
    Route::post('/radiojavan/fetch-lyric', 'crawlers\RadioJavanController@fetchLyric');
    Route::get('/youtube/{artist}/{title}/{link}', 'crawlers\YoutubeController@show');
    Route::get('/radiojavan/albums/{name}', 'crawlers\RadioJavanAlbumController@albums');
    Route::post('/radiojavan/album/store', 'crawlers\RadioJavanAlbumController@createAlbum');
    Route::post('/radiojavan/album/addTrackToDatabase', 'crawlers\RadioJavanAlbumController@addTrackToDatabase');
});

Route::get('admin/afghan123/{amount}', 'AdminController@afghan123');
Route::get('admin/login', 'AdminController@show');
Route::post('admin/login', 'AdminController@login');
Route::get('admin/check', 'AdminController@test')->middleware('admin');

Route::group(['prefix' => 'admin',  'middleware' => 'admin'], function() {
    Route::get('/', 'AdminController@index');
    Route::get('/{artist}/{album}', 'AdminController@radiojavan');
    Route::get('/{artist}', 'AdminController@radiojavanArtist');
    Route::get('/afghan123', 'AdminController@afghan123');
    Route::get('/home', 'AdminController@show');
});

Route::group(['prefix' => 'artist', 'middleware' => 'auth'], function(){
    Route::get('/', 'ArtistController@index');
    Route::get('create', 'ArtistController@create');
    Route::post('store', 'ArtistController@store');
    Route::get('search', 'ArtistController@search');
    Route::get('/{id}', 'ArtistController@show');
    Route::get('/edit/{id}', 'ArtistController@edit');
    Route::patch('/edit/{id}', 'ArtistController@update');
    Route::delete('/delete/', 'ArtistController@destroy');
    Route::patch('/publish', 'ArtistController@publish');
});

Route::group(['prefix' => 'album', 'middleware' => 'admin'], function(){
    Route::get('/{id}', 'AlbumController@show');
    Route::get('/create/{id}', 'AlbumController@create');
    Route::post('/store', 'AlbumController@store');
    Route::post('/upload', 'AlbumController@upload');
    Route::get('/play/{id}', 'AlbumController@play');
    Route::delete('/track/delete', 'AlbumController@deleteTrack');
    Route::get('edit/{id}', 'AlbumController@edit');
    Route::patch('edit/{id}', 'AlbumController@update');
    Route::delete('/delete/', 'AlbumController@destroy');
});

Route::group(['prefix' => 'user', 'middleware' => 'auth'], function(){
    Route::get('/', 'UserController@index');
    Route::get('/artist/edit/{id}', 'UserController@edit');
    Route::get('/artist/', 'UserController@artists');
    Route::get('/artist/{id}', 'UserController@show');
});

Route::group(['prefix' => 'track', 'middleware' => 'admin'], function(){
    Route::get('edit/{id}', 'TrackController@edit');
    Route::patch('edit/{id}', 'TrackController@update');
    Route::get('/{id}', 'TrackController@show');
});

Route::group(['prefix' => 'lyric', 'middleware' => 'admin'], function(){
    Route::get('/create/{id}', 'LyricController@create');
    Route::put('/create', 'LyricController@store');
    Route::get('/{id}', 'LyricController@show');
    Route::get('/edit/{id}', 'LyricController@edit');
    Route::post('/{id}', 'LyricController@store');
    Route::patch('/{id}', 'LyricController@update');
    Route::patch('/update/{id}', 'LyricController@updateSyncedLyric');
    Route::get('/delete/{id}', 'LyricController@destroy');
});

Route::get('/play/{vue_capture?}', function () {
    return view('main');
})->where('vue_capture', '[\/\w\.-]*')->middleware('auth');
