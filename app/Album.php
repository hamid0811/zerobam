<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Album extends Model implements HasMedia
{
    use HasMediaTrait;

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('large')
            ->crop(Manipulations::CROP_CENTER, 640, 640);

        $this->addMediaConversion('medium')
            ->crop(Manipulations::CROP_CENTER, 300, 300);

        $this->addMediaConversion('small')
            ->crop(Manipulations::CROP_CENTER, 64, 64);
    }
    public function tracks(){
        return $this->belongsToMany('App\Track', 'track_album')
            ->orderBy('tracks.track_number');
    }

    public function artist(){
        return $this->belongsTo('App\Artist');
    }
}
