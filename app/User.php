<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function playlists()
    {
        return $this->hasMany('App\Playlist');
    }

    public function roles()
    {
        return $this->hasOne('App\Role');
    }

    public function artists(){
        return $this->hasMany('App\Artist');
    }

    public function isAdmin() {
       return $this->roles()->where('admin', '1')->exists();
    }

    public function isArtist(){
        return $this->roles()->where('artist', '1')->exists();
    }

    public function savedTracks(){
        return $this->hasMany('App\SavedTrack');
    }
}
