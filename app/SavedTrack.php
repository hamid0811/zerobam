<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class SavedTrack extends Model
{
    use HasMediaTrait;

    public function track()
    {
        return $this->hasOne('App\Track', 'id', 'track_id');
    }

}
