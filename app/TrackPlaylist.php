<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrackPlaylist extends Model
{
    public $table = 'track_playlist';
}
