<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrackAlbum extends Model
{
    public $table = 'track_album';
}
