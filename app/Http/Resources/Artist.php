<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Artist extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $albums = array();
        foreach ($this->albums as $key => $album) {
            $albums[] = array(
                'id' => $album->id,
                'album_type' => $album->type,
                'name'=> $album->name,
                'cover' => $album->cover,
                'release_date' => $album->release_date
            );
        }

        $popularTracks = array();
        foreach ($this->popularTracks as $key => $popularTrack) {
            $popularTracks[] = array(
                'id' => $popularTrack->id,
                'title' => $popularTrack->title,
                'release_date' => $popularTrack->release_date,
                'play_count' => $popularTrack->play_count,
                'url' => $popularTrack->link,
                'album_id' => $popularTrack->album[0]->id,
                'artist_id' => $this->id,
                'artist' => $this->name,
                'cover' => $popularTrack->cover,
                'lyrics' => $popularTrack->lyrics,
            );
        }

        return [
            'id' => $this->id,
            'name' => $this->name,
            'photo' => $this->photo,
            'albums' => $albums,
            'popularTracks' => $popularTracks,
            'artist' => ['id' => $this->id, 'name' => $this->name, 'photo' => $this->photo, 'colors' => $this->colors],
        ];
    }
}
