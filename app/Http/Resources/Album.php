<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Album extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        $tracks = array();
        foreach ($this->tracks as $key => $track) {
            $tracks[] = array(
                'id' => $track->id,
                'title'=> $track->title,
                'track_number' => $track->track_number,
                'url' => $track->media_url,
                'cover' => $this->cover_url,
                'artist' => $this->artist->name,
                'lyrics' => $track->lyrics,
                'play_count' => $track->play_count,
                'colors' =>$this->colors
            );
        }

        return [
            'album' => ['id' => $this->id, 'name' => $this->name, 'cover' => $this->cover_url, 'tracks' => $tracks, 'colors' => $this->colors],
            'artist' => $this->artist,
        ];
    }
}
