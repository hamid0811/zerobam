<?php

namespace App\Http\Controllers;

use App\Album;
use App\Track;
use App\TrackAlbum;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AttachmentController extends Controller
{
    ////////////////////Store song infomration into the database///////////////
    ///////////////////////////////////////////////////////////////////////////

    public function store($file)
    {
        $getID3 = new \getID3;


        $ThisFileInfo = $getID3->analyze($file);

        $title =  $ThisFileInfo['tags']['id3v2']['title'][0];
        $artist =  $ThisFileInfo['tags']['id3v2']['artist'][0];
        $album =  $ThisFileInfo['tags']['id3v2']['album'][0];
        $trackNumber =  $ThisFileInfo['tags']['id3v2']['track_number'][0];

        // $Image='data:'.$ThisFileInfo['comments']['picture'][0]['image_mime'].';charset=utf-8;base64,'.base64_encode($ThisFileInfo['comments']['picture'][0]['data']);

        $songsData = array();
        $songsData['title'] = ucwords($title);
        $songsData['artist'] = ucwords($artist);
        $songsData['album'] = ucwords($album);
        $songsData['track_number'] = $trackNumber;

        return $songsData;
    }
}
