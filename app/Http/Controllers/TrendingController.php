<?php

namespace App\Http\Controllers;

use App\Trending;
use Illuminate\Http\Request;

class TrendingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Trending  $trending
     * @return \Illuminate\Http\Response
     */
    public function show(Trending $trending)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Trending  $trending
     * @return \Illuminate\Http\Response
     */
    public function edit(Trending $trending)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Trending  $trending
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Trending $trending)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Trending  $trending
     * @return \Illuminate\Http\Response
     */
    public function destroy(Trending $trending)
    {
        //
    }
}
