<?php

namespace App\Http\Controllers;

use App\Album;
use App\Artist;
use App\Lyric;
use App\Track;
use App\TrackAlbum;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class AdminController extends Controller
{
    public function index()
    {
        return view('admins.index');
    }

    public function afghan123($amount){
        $artists = array();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://www.afghan123.com/music/');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $data = curl_exec($ch);

        $doc = new \DOMDocument();
        @$doc->loadHTML('<?xml encoding="utf-8" ?>'.$data);
        $xpath = new \DOMXpath($doc);

        $listViews = $xpath->query('//a[@class="list-group-item"]');

        foreach ($listViews as $tag) {
            $href =  $tag->getAttribute('href');
            $href = trim($href, "/");
            $value = str_replace('music/', '', $href);
            $artists[] = ucwords(str_replace('-', ' ', $value));
        }

        $artists = array_slice($artists, $amount);

        foreach ($artists as $key => $artist) {
            $artistName = trim($artist);
            $artist = Artist::where('name', $artistName)->first();
            if ($artist == null) {

                $name = str_replace(' ', '-', $artistName);
                $url = 'https://www.afghan123.com/music/'.$name;
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url.'/');
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                $data = curl_exec($ch);

                $doc = new \DOMDocument();
                @$doc->loadHTML('<?xml encoding="utf-8" ?>'.$data);
                $xpath = new \DOMXpath($doc);
                $artistImage = "https://www.afghan123.com".$xpath->query("//img")->item(0)->getAttribute('src');
                $artists[] = $artistImage;

                $artist = new Artist;
                $artist->name = $artistName;
                $artist->is_published = 1;
                $artist->photo = $artistImage;

                $contents = file_get_contents($artistImage);
                Storage::disk('public')->put($name.'.jpg', $contents);
                $path = Storage::disk('public')->path($name.'.jpg');

                $img = Image::make($path);
                Storage::disk('public')->put($name.'.jpg', (string) $img->encode());
                $path = Storage::disk('public')->path($name.'.jpg');
                $artist->addMedia($path)->toMediaCollection('photo');

                $artist->save();
            }

        }

        // return $artists;
    }

    public function view(){
        $scripts = array();
        $artists = array();

        $ch = curl_init();
        $data = array('user' => 'xxx', 'password' => 'yyy');
        curl_setopt($ch, CURLOPT_URL, 'https://www.afghan123.com/music/');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $data = curl_exec($ch);

        $doc = new \DOMDocument();
        @$doc->loadHTML('<?xml encoding="utf-8" ?>'.$data);
        $xpath = new \DOMXpath($doc);

        $listViews = $xpath->query('//font');

        // $script_tags = $xpath->query('//body//script[not(@src)]');
        foreach ($listViews as $tag) {
            $scripts[] =  utf8_encode($tag->nodeValue);
        }
        // return $scripts;
        // $scripts = explode(";", $scripts[2]);
        // if (preg_match("/'(.*?)'/", $scripts[0], $match) == 1) {
        //     return $match[1];
        // }
        $output = array_slice($scripts, 4);
        foreach (range(1, count($output), 2) as $key) {
          unset($output[$key]);
        }
        array_pop($output);
        $output = array_slice($output, 340);
        foreach ($output as $key => $name) {
            $artists[] = Artist::where('name', $name)->first();
        }

        foreach ($artists as $key => $artist) {
            $name = str_replace(' ', '-', $artist->name);
            return $name;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://www.afghan123.com/music/'.$name);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $data = curl_exec($ch);

            $doc = new \DOMDocument();
            @$doc->loadHTML('<?xml encoding="utf-8" ?>'.$data);
            $xpath = new \DOMXpath($doc);

            $listViews = "https://www.afghan123.com".$xpath->query("//img")->item(0)->getAttribute('src');
            $artists[] = $listViews;
        }
        return $artists;
        return  view('admins.index');
    }

    public function DOMinnerHTML($element)
    {
        $innerHTML = "";
        $children = $element->childNodes;
        foreach ($children as $child)
        {
            $tmp_dom = new \DOMDocument();
            $tmp_dom->appendChild($tmp_dom->importNode($child, true));
            $innerHTML.=trim($tmp_dom->saveHTML());
        }
        return $innerHTML;
    }

    public function strip_tags_content($text, $tags = '', $invert = FALSE) {

        preg_match_all('/<(.+?)[\s]*\/?[\s]*>/si', trim($tags), $tags);
        $tags = array_unique($tags[1]);
        if(is_array($tags) AND count($tags) > 0) {
            if($invert == FALSE) {
                return preg_replace('@<(?!(?:'. implode('|', $tags) .')\b)(\w+)\b.*?>.*?</\1>@si', '', $text);
            }
            else {
                return preg_replace('@<('. implode('|', $tags) .')\b.*?>.*?</\1>@si', '', $text);
            }
        }
        elseif($invert == FALSE) {
            return preg_replace('@<(\w+)\b.*?>.*?</\1>@si', '', $text);
        }
        return $text;
    }

    public function lyrics()
    {
        $lyrics = array('Title'=> ['Dari'=> ''], 'Artist' => ['Dari' => '', 'English' => ''],'Dari' => [],'English' => []);
        $allLyrics = array();



        for ( $i = 0; $i < 3; $i++) {
            $url = "http://www.afghansonglyrics.com/Artist/Lyrics/".$i;

            $file_headers = @get_headers($url);
            if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
                $exists = false;
            }else{

                $lyrics = array('Title'=> ['Dari'=> ''], 'Artist' => ['Dari' => '', 'English' => ''],'Dari' => [],'English' => []);

                $data = file_get_contents($url);
                $doc = new \DOMDocument();
                libxml_use_internal_errors(true);
                @$doc->loadHTML($data);
                $finder = new \DomXPath($doc);
                $node = $finder->query("//*[contains(@class, 'lyric')]");


                $titles = $doc->getElementsByTagName('h1');
                foreach ($titles as $key => $title) {
                    $title = preg_replace_callback("/(&#[0-9]+;)/", function($m) { return mb_convert_encoding($m[1], "UTF-8", "HTML-ENTITIES"); }, $this->DOMinnerHTML($title));
                    $myArray = explode("<br>",$title);
                    $artist = $this->strip_tags_content($myArray[0], '<a>');

                    $dariArtist = $artist = $this->strip_tags_content($myArray[1], '<a>');
                    $dariArtist = strip_tags($dariArtist);

                    $title = $this->strip_tags_content($myArray[0], '<a>');
                    $artist = strip_tags($foo = preg_replace('/\s+/', ' ', $artist));


                    if ($key == 1) {
                        $lyrics['Artist']['English'] = preg_replace('/\s+/', ' ', $artist);
                        $lyrics['Title']['English'] = preg_replace('/\s+/', ' ', $title);
                    }else{
                        $lyrics['Title']['Dari'] = preg_replace('/\s+/', ' ', $title);
                        $lyrics['Artist']['Dari'] = preg_replace('/\s+/', ' ', $dariArtist);
                    }


                }


                for ($counter=1; $counter < 3  ; $counter++) {
                    $dariLyric = $doc->saveHTML($node->item($counter));
                    $lyric = print_r($dariLyric,true);
                    $lyric = strip_tags($lyric,'<br>');
                    $lyric = $this->strip_tags_content($lyric, '<p>');
                    $myArray = explode("\r\n",$lyric);
                    array_shift($myArray);
                    array_pop($myArray);

                    if ($counter == 1) {
                        foreach ($myArray as $key => $lyric) {
                            $lyricLine = explode('<br>', $lyric);
                            $paragraph = array();
                            foreach ($lyricLine as $key => $line) {
                                array_push($paragraph, trim($line));
                            }
                            array_push($lyrics['Dari'], $paragraph);
                        }
                    }else{
                        foreach ($myArray as $key => $lyric) {
                            $lyricLine = explode('<br>', $lyric);
                            $paragraph = array();
                            foreach ($lyricLine as $key => $line) {
                                array_push($paragraph, trim($line));
                            }
                            array_push($lyrics['English'], $paragraph);
                        }
                    }
                }

                $fullLine = "";
                foreach ($lyrics['Dari'] as $key => $paragraph) {
                    foreach ($paragraph as $key => $line) {
                        $fullLine .= $line."\r\n";
                        if (next($paragraph)==false) $fullLine .= "\r\n";
                    }
                }
                $lyric = new Lyric;
                $lyric->user_id = auth()->id();
                $lyric->title_dari = trim($lyrics['Title']['Dari']);
                $lyric->artist_dari = trim($lyrics['Artist']['Dari']);
                $lyric->title_english = trim($lyrics['Title']['English']);
                $lyric->artist_english = trim($lyrics['Artist']['English']);
                $lyric->lyric = $fullLine;
                $lyric->save();
            }
            array_push($allLyrics, $lyrics);
        }
        return $allLyrics;



        // $lyrics = array();


        // for ( $i = 0; $i < 2; $i++) {

        //     $url = "http://www.afghansonglyrics.com/Artist/Lyrics/".$i;

        //     $file_headers = @get_headers($url);
        //     if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
        //         $exists = false;
        //     }
        //     else {
        //         $data = file_get_contents($url);

        //         $dom = new \DOMDocument();
        //         @$dom->loadHTML($data);

        //         $nodes = $dom->getElementsByTagName('p');

        //         $lyric = array();

        //         foreach ($nodes as $key => $node) {
        //             if ($key > 1) {
        //                 $lyricLine = preg_replace_callback("/(&#[0-9]+;)/", function($m) { return mb_convert_encoding($m[1], "UTF-8", "HTML-ENTITIES"); }, $this->DOMinnerHTML($node));
        //                 $myArray = explode("<br>",$lyricLine);
        //                 array_push($lyric, $myArray);
        //             }
        //         }
        //         array_pop($lyric);
        //         array_push($lyrics, $lyric);


        //         $titles = $dom->getElementsByTagName('h1');
        //         foreach ($titles as $key => $title) {
        //             $title = preg_replace_callback("/(&#[0-9]+;)/", function($m) { return mb_convert_encoding($m[1], "UTF-8", "HTML-ENTITIES"); }, $this->DOMinnerHTML($title));
        //             // return $title;
        //             $myArray = explode("<br>",$title);
        //             $artist = $this->strip_tags_content($myArray[1], '<a>');
        //             $artist = strip_tags($foo = preg_replace('/\s+/', ' ', $artist));
        //             // return $artist;
        //             array_push($lyrics[0], $artist);
        //         }
        //     }
        // }
        // return $lyrics;
        return  view('admins.index', compact(['lyric']));
    }

    public function getElementsByClassName($dom, $ClassName, $tagName=null) {
        if($tagName){
            $Elements = $dom->getElementsByTagName($tagName);
        }else {
            $Elements = $dom->getElementsByTagName("*");
        }
        $Matched = array();
        for($i=0;$i<$Elements->length;$i++) {
            if($Elements->item($i)->attributes->getNamedItem('class')){
                if($Elements->item($i)->attributes->getNamedItem('class')->nodeValue == $ClassName) {
                    $Matched[]=$Elements->item($i);
                }
            }
        }
        return $Matched;
    }

    public function bia2(){

        $artist = array('albums' => []);

        $url = "https://www.bia2.com/artist/shadmehr-aghili";

        $file_headers = @get_headers($url);
        if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
            $exists = false;
        }else{
            $data = file_get_contents($url);

            $doc = new \DOMDocument();
            @$doc->loadHTML($data);

            $xpath = new \DOMXpath($doc);
            $articles = $xpath->query('//div[@class="block-holder"]');
            $albums = [];
            foreach($articles as $key => $container) {

                $arr = $container->getElementsByTagName('a');

                foreach ($arr as $link) {
                    $href =  $link->getAttribute("href");
                    $value = trim(preg_replace("/[\r\n]+/", " ", $link->nodeValue));

                    if ($value !== "" && $key < 2) {
                        if ($key == 0) {
                            $album = Array(
                                'title' => $value,
                                'link' => $href,
                                'image_url' => '',
                            );
                            $albums[] = $album;
                            $artist['albums'] = $albums;
                        }
                    }
                }

                $images = $container->getElementsByTagName('img');
                $i = 0;
                foreach ($images as $image) {
                    $image =  $image->getAttribute("src");
                    if ($image !== "" && $key < 2) {
                        if ($key == 0) {
                           $artist['albums'][$i]['image_url'] = str_replace('300', '500', $image);
                           $i++;
                        }
                    }
                }
            }

            return $artist;
        }
    }

    public function radiojavan($artistName, $albumName){
        $artist = ucwords($artistName);
        $albumName = ucwords($albumName);
        $album = ['artworks' => [], 'tracks' => [], 'track_urls' => []];

        $infos = [];

        $url = "https://www.radiojavan.com/mp3s/album/Mehdi-Yarrahi-Mesle-Mojasameh";
        $file_headers = @get_headers($url);
        if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
            $exists = false;
        }else{
            $scripts = array();

            $ch = curl_init();
            $data = array('user' => 'xxx', 'password' => 'yyy');
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $data = curl_exec($ch);

            $doc = new \DOMDocument();
            @$doc->loadHTML('<?xml encoding="utf-8" ?>'.$data);
            $xpath = new \DOMXpath($doc);

            $listViews = $xpath->query('//ul[@class="listView"]');

            $script_tags = $xpath->query('//body//script[not(@src)]');
            foreach ($script_tags as $tag) {
                $scripts[] = $tag->nodeValue;
            }
            $scripts = explode(";", $scripts[2]);
            if (preg_match("/'(.*?)'/", $scripts[0], $match) == 1) {
                return $match[1];
            }


            foreach ($listViews as $key => $listView) {
                $trackLinks = $listView->getElementsByTagName("a");
                $images = $listView->getElementsByTagName('img');
                $trackInfos = $listView->getElementsByTagName('span');

                foreach ($trackInfos as $key => $trackInfo) {
                    if ($trackInfo->nodeValue && strpos($trackInfo->nodeValue, 'plays') === false && strpos($trackInfo->nodeValue, $artist) === false) {

                        $hostUrl = 'https://host2.rjmusicmedia.com/media/mp3/mp3-256/'.str_replace(" ", "-", $artist).'-'.str_replace(" ", "-", $trackInfo->nodeValue).'.mp3';
                        $file_headers = @get_headers($hostUrl);
                        if($file_headers[0] !== 'HTTP/1.1 200 OK') {
                            $hostUrl = 'https://host1.rjmusicmedia.com/media/mp3/mp3-256/'.str_replace(" ", "-", $artist).'-'.str_replace(" ", "-", $trackInfo->nodeValue).'.mp3';
                        }

                        array_push($album['tracks'],$trackInfo->nodeValue);
                        array_push($album['track_urls'], str_replace(' ', '-', $hostUrl));
                    }
                }

                foreach ($images as $key => $image) {
                    $image = $image->getAttribute('src');
                    array_push($album['artworks'], str_replace("-thumb", "", $image));
                }
            }

            return $album;

            // $existingAlbum = Album::where('name', $albumName)->first();
            // $existingArtist = Artist::where('name', $artist)->first();


            // if ($existingArtist) {
            //     $newAlbum = new Album;
            //     $newAlbum->name = $albumName;
            //     $newAlbum->addMediaFromUrl($album['artworks'][0])->toMediaCollection('cover');
            //     $newAlbum->type = 'album';
            //     $newAlbum->artist_id = $existingArtist->id;
            //     $newAlbum->save();

            //     foreach ($album['tracks'] as $key => $track) {
            //         $track = new Track;
            //         $track->artist_id = $existingArtist->id;
            //         $track->addMediaFromUrl($album['track_urls'][$key])->toMediaCollection('track');
            //         $track->title = $album['tracks'][$key];
            //         $track->url = $album['track_urls'][$key];
            //         $track->save();

            //         $trackAlbum = new TrackAlbum;
            //         $trackAlbum->track_id = $track->id;
            //         $trackAlbum->album_id = $newAlbum->id;
            //         $trackAlbum->save();
            //     }
            //     return 'Added new one new album. Artist already exists.';
            // }else{
            //     $newArtist = new Artist;
            //     $newArtist->name = $artist;
            //     $newArtist->is_published = 1;
            //     $newArtist->addMediaFromUrl($album['artworks'][0])->toMediaCollection('photo');
            //     $newArtist->save();

            //     $newAlbum = new Album;
            //     $newAlbum->name = $albumName;
            //     $newAlbum->addMediaFromUrl($album['artworks'][0])->toMediaCollection('cover');
            //     $newAlbum->type = 'album';
            //     $newAlbum->artist_id = $newArtist->id;
            //     $newAlbum->save();

            //     foreach ($album['tracks'] as $key => $track) {
            //         $track = new Track;
            //         $track->artist_id = $newArtist->id;
            //         $track->addMediaFromUrl($album['track_urls'][$key])->toMediaCollection('track');
            //         $track->title = $album['tracks'][$key];
            //         $track->url = $album['track_urls'][$key];
            //         $track->save();

            //         $trackAlbum = new TrackAlbum;
            //         $trackAlbum->track_id = $track->id;
            //         $trackAlbum->album_id = $newAlbum->id;
            //         $trackAlbum->save();
            //     }
            // }

            // return 'Added new artist and one new album for the artist';
        }
    }

    public function radiojavanArtist($artist){
        $artist = ucwords($artist);
        $trackName = '';

        $script = array();

        $album = ['artworks' => [], 'tracks' => [], 'urls' => []];

        $url = "https://www.radiojavan.com/artist/".str_replace(" ", "+", $artist);

        $file_headers = @get_headers($url);
        if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
            $exists = false;
        }else{
            $data = file($url);
            $doc = new \DOMDocument();
            @$doc->loadHTML('<?xml encoding="utf-8" ?>'.$data);
            $xpath = new \DOMXpath($doc);

            $listViews = $xpath->query('//div[@class="itemContainer"]');
            foreach ($listViews as $key => $listView) {
                dd($listView);
                $trackLinks = $listView->getElementsByTagName("a");
                $images = $listView->getElementsByTagName('img');
                $trackInfos = $listView->getElementsByTagName('span');

                foreach ($trackInfos as $key => $trackInfo) {
                    if ($trackInfo->nodeValue && strpos($trackInfo->nodeValue, 'plays') === false && strpos($trackInfo->nodeValue, $artist) === false) {
                        $trackName = $trackInfo->nodeValue;
                        array_push($album['tracks'],$trackInfo->nodeValue);
                        $hostUrl = 'https://host2.rjmusicmedia.com/media/mp3/mp3-256/'.str_replace(" ", "-", $artist).'-'.str_replace(" ", "-", $trackInfo->nodeValue).'.mp3';
                        $file_headers = @get_headers($hostUrl);
                        if($file_headers[0] !== 'HTTP/1.1 200 OK') {
                            $hostUrl = 'https://host1.rjmusicmedia.com/media/mp3/mp3-256/'.str_replace(" ", "-", $artist).'-'.str_replace(" ", "-", $trackInfo->nodeValue).'.mp3';
                        }
                        array_push($album['urls'], str_replace(' ', '-', $hostUrl));
                    }
                }

                foreach ($images as $key => $image) {
                    $image = $image->getAttribute('src');
                    array_push($album['artworks'], $image);
                }
            }

            // return $album;


            // $artistInDatabase = Artist::where('name', $artist)->first();
            // return $artistInDatabase;

            // if ($artistInDatabase) {
            //     foreach ($album['tracks'] as $key => $track) {
            //         $TrackInDatabase = Track::where(['title' => $track, 'artist_id' => $artistInDatabase->id])->first();
            //         if (!$TrackInDatabase) {
            //             $newAlbum = new Album;
            //             $newAlbum->name = $track;
            //             $newAlbum->addMediaFromUrl($album['artworks'][$key])->toMediaCollection('cover');
            //             $newAlbum->type = 'single';
            //             $newAlbum->artist_id = $artistInDatabase->id;
            //             $newAlbum->save();

            //             $track = new Track;
            //             $track->artist_id = $artistInDatabase->id;
            //             $track->addMediaFromUrl($album['urls'][$key])->toMediaCollection('track');
            //             $track->title = $album['tracks'][$key];
            //             $track->url = $album['urls'][$key];
            //             $track->save();

            //             $trackAlbum = new TrackAlbum;
            //             $trackAlbum->track_id = $track->id;
            //             $trackAlbum->album_id = $newAlbum->id;
            //             $trackAlbum->save();
            //         }
            //     }
            //     return 'Artist already exists. Adding track operation finished';
            // }else{
            //     $newArtist = new Artist;
            //     $newArtist->name = $artist;
            //     $newArtist->is_published = 1;
            //     $newArtist->addMediaFromUrl($album['artworks'][0])->toMediaCollection('photo');
            //     $newArtist->save();


            //     foreach ($album['tracks'] as $key => $track) {
            //         $newAlbum = new Album;
            //         $newAlbum->name = $track;
            //         $newAlbum->addMediaFromUrl($album['artworks'][$key])->toMediaCollection('cover');
            //         $newAlbum->type = 'single';
            //         $newAlbum->artist_id = $newArtist->id;
            //         $newAlbum->save();

            //         $track = new Track;
            //         $track->artist_id = $newArtist->id;
            //         $track->addMediaFromUrl($album['urls'][$key])->toMediaCollection('track');
            //         $track->title = $album['tracks'][$key];
            //         $track->url = $album['urls'][$key];
            //         $track->save();

            //         $trackAlbum = new TrackAlbum;
            //         $trackAlbum->track_id = $track->id;
            //         $trackAlbum->album_id = $newAlbum->id;
            //         $trackAlbum->save();
            //     }
            // }

            // return 'Added new artist and all its singles.';
        }
    }

    public function afghanMusik($afghanMusik){
        return "hello";

    }
}
