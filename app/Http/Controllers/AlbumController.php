<?php

namespace App\Http\Controllers;

use App\Album;
use App\Artist;
use App\Helpers\GeneralHelper;
use App\Track;
use App\TrackAlbum;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use League\ColorExtractor\Color;
use League\ColorExtractor\ColorExtractor;
use League\ColorExtractor\Palette;
use ColorThief\ColorThief;

class AlbumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $artist = Artist::find($id);
        return view('admins.albums.create', compact('artist'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $album = new Album;
        $album->type = $request->type;
        $album->name = $request->name;

        ///////////////////////////////////////////////////////////////
        ////////////////Extract Color From Image///////////////////////
        $img = file_get_contents($request->photo);
        $palette = ColorThief::getPalette($img, 8);
        /////////////////////////////////////////////////////////////////

        $media = $album->addMedia($request->photo)->withCustomProperties(['colors' => $palette])->toMediaCollection('cover');
        $album->cover = $media->id;
        $album->artist_id = $request->artist_id;
        $album->release_date = $request->release_date;
        $album->save();

        return redirect('/album/'.$album->id)->with('status', 'New Album Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function show(Album $album, $id)
    {
        $album = $album->where('id', $id)->with('tracks', 'artist')->first();
        $totalPlays = $album->tracks()->sum('play_count');
        $album['totalPlays'] = $totalPlays;
        return view('admins.albums.show', compact('album'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function edit(Album $album, $id)
    {
        $album = $album->find($id);
        $media = $album->getFirstMedia('cover');
        return view('admins.albums.edit', compact('album'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Album $album, $id)
    {
        $album = $album->find($id);
        $album->type = $request->type;
        $album->name = $request->name;
        if($request->hasFile('cover')){
            ///////////////////////////////////////////////////////////////
            ////////////////Extract Color From Image///////////////////////
            $img = file_get_contents($request->cover);
            $palette = ColorThief::getPalette($img, 8);
            /////////////////////////////////////////////////////////////////

            $media = $album->getFirstMedia('cover');
            if($media != null){
             $media->delete();
            }
            $album->addMedia($request->cover)->withCustomProperties(['colors' => $palette])->toMediaCollection('cover');

        }
        $album->save();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function destroy(Album $album, Request $request)
    {
        $album = $album->find($request->album_id);
        $album->tracks()->delete();
        $album->tracks()->detach();
        $media = $album->getFirstMedia('cover');
        if ($media) {
            $media->delete();
        }
        $album->delete();

        return redirect('/artist/'.$album->artist->id);


    }

    public function upload(Request $request)
    {
        $artistID = $request->get('artistID');
        $albumID = $request->get('albumID');

        if($files=$request->file('files')){
            foreach($files as $file){

                $name= $file->getClientOriginalName();

                $songInfo = GeneralHelper::getSongInfo($file);

                $track = new Track;
                $track->artist_id = $artistID;
                $track->addMedia($file)->toMediaCollection('track');
                $track->title = $songInfo['title'];
                $track->track_number = $songInfo['track_number'];
                $track->save();

                $track_album = new TrackAlbum;
                $track_album->track_id = $track->id;
                $track_album->album_id = $albumID;
                $track_album->save();


            }
        }
        return ($request);
    }

    public function play($id)
    {
        $track = Track::find($id);
        $url = Storage::disk('do_spaces')->temporaryUrl(
            'artists/'.$track->artist_id.'/audios/'.$track->url, now()->addMinutes(1)
        );

        return ($url);
    }

    public function deleteTrack(Request $request)
    {
        // dd($request);
        $track = Track::find($request->track_id);
        Storage::disk('do_spaces')->delete('/artists/'.$track->artist_id.'/audios/'.$track->url);
        $track_album = TrackAlbum::where(['track_id' => $track->id, 'album_id' => $request->album_id]);
        $track_album->delete();
        $track->delete();

        return back()->with('status', 'Track deleted!');
    }
}
