<?php

namespace App\Http\Controllers\crawlers;

use App\Album;
use App\Artist;
use App\Http\Controllers\Controller;
use App\Lyric;
use App\Track;
use App\TrackAlbum;
use ColorThief\ColorThief;
use Illuminate\Http\Request;

class RadioJavanController extends Controller
{
    public function index()
    {
        return view('admins.crawlers.radiojavan.index');
    }

    public function albums($artist){
        $artist = ucwords($artist);
        $trackName = '';
        $allAlbums;

        $url = "https://www.radiojavan.com/artist/".str_replace(" ", "+", $artist);

        $file_headers = @get_headers($url);

                if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
            return "failed opening url";
        }else{
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $data = curl_exec($ch);

            $doc = new \DOMDocument();
            @$doc->loadHTML('<?xml encoding="utf-8" ?>'.$data);
            $xpath = new \DOMXpath($doc);

            $names = [];
            $mp3s;
            $albums = [];

            $hasAlbum = $xpath->evaluate('boolean(//h2[contains(@id,"artist_ablums")])');
            if ($hasAlbum) {
                $albums = $xpath->query('//div[@class="grid"]')[0];

                $albumLinks = $albums->getElementsByTagName('a');
                foreach ($albumLinks as $key => $link) {
                    $allAlbums[$key]['link'] = $link->getAttribute('href');
                }

                $albumNames = $albums->getElementsByTagName('span');
                foreach ($albumNames as $key => $name) {
                    if ($name->nodeValue !== $artist) {
                        $names[] = $name->nodeValue;
                    }
                }
                foreach ($names as $key => $name) {
                    $allAlbums[$key]['name']  = $name;
                }

                $albumImages = $albums->getElementsByTagName("img");
                foreach ($albumImages as $key => $image) {
                    $allAlbums[$key]['image']  = $image->getAttribute('src');
                }

                //////////////////////get albums tracks lists//////////////////////////
                ///////////////////////////////////////////////////////////////////////

                $artistAlbums;


                foreach ($allAlbums as $key => $album) {
                    if (isset($album["link"])) {
                        $artistAlbums[$key] = $album;
                        $artistAlbums[$key]['artist'] = $artist;

                        $albumTracks = $this->fetchAlbumTracks($album);
                        $currentAlbum = [];
                        foreach ($albumTracks as $index => $track) {
                            $currentAlbum[$index]['name'] = $track;
                            $currentAlbum[$index]['artist'] = $artist;
                            $currentAlbum[$index]['artwork'] = $album['image'];
                            $currentAlbum[$index]['link'] = "mp3s/mp3/".str_replace(" ", "-", $artist)."-".str_replace(" ", "-", $track);
                        }
                        $artistAlbums[$key]['tracks'] = $currentAlbum;
                    }
                }

                return $artistAlbums;
            }
        }
    }

    public function singles($artist)
    {
        $artist = ucwords($artist);
        $trackName = '';
        $allAlbums;


        $url = "https://www.radiojavan.com/artist/".str_replace(" ", "+", $artist);

        $file_headers = @get_headers($url);
        if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
            return "failed opening url";
        }else{
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $data = curl_exec($ch);

            $doc = new \DOMDocument();
            @$doc->loadHTML('<?xml encoding="utf-8" ?>'.$data);
            $xpath = new \DOMXpath($doc);

            $names = [];
            $mp3s;
            $albums = [];

            $hasAlbum = $xpath->evaluate('boolean(//h2[contains(@id,"artist_ablums")])');
            if ($hasAlbum) {
                $albums = $xpath->query('//div[@class="grid"]')[0];

                $albumLinks = $albums->getElementsByTagName('a');
                foreach ($albumLinks as $key => $link) {
                    $allAlbums[$key]['link'] = $link->getAttribute('href');
                }

                $albumNames = $albums->getElementsByTagName('span');
                foreach ($albumNames as $key => $name) {
                    if ($name->nodeValue !== $artist) {
                        $names[] = $name->nodeValue;
                    }
                }
                foreach ($names as $key => $name) {
                    $allAlbums[$key]['name']  = $name;
                }

                $albumImages = $albums->getElementsByTagName("img");
                foreach ($albumImages as $key => $image) {
                    $allAlbums[$key]['image']  = $image->getAttribute('src');
                }
                //////////////////////get albums tracks lists//////////////////////////
                ///////////////////////////////////////////////////////////////////////
                $mp3s = $this->fetchAllMp3s($artist);

                $artistAlbums;


                foreach ($allAlbums as $key => $album) {
                    if (isset($album["link"])) {
                        $artistAlbums[] = $this->fetchAlbumTracks($album);
                    }
                }


                $allAlbumTracks = [];
                foreach ($artistAlbums as $key => $album) {
                    foreach ($album as $key => $track) {
                        $allAlbumTracks[] = $track;
                    }
                }


                /////////////////////Remove mp3s which belong to an album////////////////
                /////////////////////////////////////////////////////////////////////////
                $positions = [];
                foreach ($allAlbumTracks as $key => $track) {
                    foreach ($mp3s as $key => $mp3) {
                        if ($track == $mp3['name']) {
                            $positions[] = $key;
                            break;
                        }
                    }
                }
                foreach ($positions as $key => $position) {
                    unset($mp3s[$position]);
                }
                ///////////////////////////////////////////////////////////////////////////

            }else{
                $mp3s = $this->fetchAllMp3s($artist);
            }

            $singles = $this->fetchSingles($mp3s);


            $singles = array_values($singles);
            return $singles;
        }
    }

    public function fetchAlbumTracks($album)
    {
        $tracks = [];

        $url = 'https://www.radiojavan.com/'.$album['link'];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $data = curl_exec($ch);
        $doc = new \DOMDocument();
        @$doc->loadHTML('<?xml encoding="utf-8" ?>'.$data);
        $xpath = new \DOMXpath($doc);

        $songInfos = $xpath->query('//div[@class="songInfo"]');

        foreach ($songInfos as $key => $songInfo) {
            $trackInfoElements = $songInfo->getElementsByTagName('span');
            foreach ($trackInfoElements as $key => $element) {
                if ($key == 1) {
                    $tracks[] = $element->nodeValue;
                }
            }
        }
        return $tracks;
    }

    public function fetchAllMp3s($artist)
    {
        $mp3s;
        $url = "https://www.radiojavan.com/search?artist=".str_replace(" ", "+", $artist)."&type=mp3";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $data = curl_exec($ch);

        $doc = new \DOMDocument();
        @$doc->loadHTML('<?xml encoding="utf-8" ?>'.$data);
        $xpath = new \DOMXpath($doc);

        $names;
        $artists;
        $artworks;
        $links;
        $listViews = $xpath->query('//div[@class="itemContainer"]');
        foreach ($listViews as $key => $listView) {

            $trackLinks = $listView->getElementsByTagName("a");
            foreach ($trackLinks as $key => $link) {
                $links[] = $link->getAttribute('href');
            }

            $images = $listView->getElementsByTagName('img');
            $trackInfos = $listView->getElementsByTagName('span');

            foreach ($trackInfos as $key => $trackInfo) {
                if ($key == "1") {
                    $trackName = $trackInfo->nodeValue;
                    $names[] = $trackInfo->nodeValue;
                }
                if ($key == "0") {
                    $trackName = $trackInfo->nodeValue;
                    $artists[] = $trackInfo->nodeValue;
                }
            }

            foreach ($images as $key => $image) {
                $image = $image->getAttribute('src');
                $artworks[] = $image;
            }
        }
        foreach ($names as $key => $name) {
            $mp3s[$key]['name'] = $name;
            $mp3s[$key]['artwork'] = $artworks[$key];
            $mp3s[$key]['artist'] = $artists[$key];
            $mp3s[$key]['link'] = $links[$key];
        }
        return $mp3s;
    }

    public function fetchSingles($singles)
    {
        $trackUrls = [];

        foreach ($singles as $key => $single) {
            $url = "https://www.radiojavan.com/".$single['link'];

            $file_headers = @get_headers($url);
            if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
                return "Url coudn't be reached";
            }else{
                $scripts = array();

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                $data = curl_exec($ch);

                $doc = new \DOMDocument();
                @$doc->loadHTML('<?xml encoding="utf-8" ?>'.$data);
                $xpath = new \DOMXpath($doc);

                $listViews = $xpath->query('//ul[@class="listView"]');

                $script_tags = $xpath->query('//body//script[not(@src)]');
                foreach ($script_tags as $tag) {
                    $scripts[] = $tag->nodeValue;
                }

                $scripts = explode(";", $scripts[2]);
                preg_match("/'(.*?)'/", $scripts[0], $match);

                if (preg_match("/'(.*?)'/", $scripts[0], $match) == 1) {

                    /////////////Check to see on which host does the file exist///////////////////////////
                    //////////////////////////////////////////////////////////////////////////////////////
                    $host1 = "https://host1.rjmusicmedia.com/media/".$match[1].'.mp3';
                    $host2 = "https://host2.rjmusicmedia.com/media/".$match[1].'.mp3';
                    $file_headers = @get_headers($host1);
                    if(in_array("Content-Type: audio/mpeg", $file_headers)) {
                        $trackUrls[$key]['track_url'] = "https://host1.rjmusicmedia.com/media/".$match[1].'.mp3';
                    }else{
                        $trackUrls[$key]['track_url'] = "https://host2.rjmusicmedia.com/media/".$match[1].'.mp3';
                    }
                    ////////////////////////////////////////////////////////////////////////////////////////

                    $trackUrls[$key]['artwork'] = $single['artwork'];
                    $trackUrls[$key]['title'] = $single['name'];
                    $trackUrls[$key]['artist'] = $single['artist'];
                    $trackUrls[$key]['link'] = $single['link'];
                }
            }

        }

        return $trackUrls;
    }

    public function saveToDataBase(Request $request)
    {
        $artist = $request->single;
        $track = $request->single;
        $artistInDatabase = Artist::where('name', $artist['artist'])->first();

        if ($artistInDatabase) {
            ////////////////Extract Color From Image///////////////////////
            $palette = ColorThief::getPalette($track['artwork'], 8);
            /////////////////////////////////////////////////////////////////

            $TrackInDatabase = Track::where(['title' => $track['title'], 'artist_id' => $artistInDatabase->id])->first();
            if (!$TrackInDatabase) {
                $newAlbum = new Album;
                $newAlbum->name = $track['title'];
                $newAlbum->addMediaFromUrl($track['artwork'])->withCustomProperties(['colors' => $palette])->toMediaCollection('cover');
                $newAlbum->type = 'single';
                $newAlbum->artist_id = $artistInDatabase->id;
                $newAlbum->save();

                $newTrack = new Track;
                $newTrack->artist_id = $artistInDatabase->id;
                $newTrack->addMediaFromUrl($track['track_url'])->toMediaCollection('track');
                $newTrack->title = $track['title'];
                $newTrack->url = $track['track_url'];
                $newTrack->save();

                $trackAlbum = new TrackAlbum;
                $trackAlbum->track_id = $newTrack->id;
                $trackAlbum->album_id = $newAlbum->id;
                $trackAlbum->save();

                $lyric = new Lyric;
                $lyric->user_id = auth()->id();
                $lyric->track_id = $newTrack->id;
                $lyric->lyric = $track['lyric'];
                $lyric->save();
            }
            return 'Track added to the database.';
        }
    }

    public function fetchLyric(Request $request){
        $url = "https://www.radiojavan.com".$request['single']['link'];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $data = curl_exec($ch);

        $doc = new \DOMDocument();
        @$doc->loadHTML('<?xml encoding="utf-8" ?>'.$data);
        $xpath = new \DOMXpath($doc);

        $hasFarsiLyric = $xpath->evaluate('boolean(//div[contains(@class,"lyricsFarsi text-center")])');
        $hasEnglishLyric = $xpath->evaluate('boolean(//div[contains(@class,"lyricsEnglish text-center")])');

        if ($hasFarsiLyric) {
            $lyric = $xpath->query('//div[@class="lyricsFarsi text-center"]')[0];

            $fullLyric = $doc->saveHTML($lyric);

            $result = [];
            $result['lyric'] =  trim(strip_tags($fullLyric));
            $result['index'] = $request['index'];
            return $result;
        }else if ($hasEnglishLyric) {
            $lyric = $xpath->query('//div[@class="lyricsEnglish text-center"]')[0];

            $fullLyric = $doc->saveHTML($lyric);

            $result = [];
            $result['lyric'] =  trim(strip_tags($fullLyric));
            $result['index'] = $request['index'];
            return $result;
        }

    }
}
