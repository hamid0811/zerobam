<?php

namespace App\Http\Controllers\crawlers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class YoutubeController extends Controller
{
    public function show($artist, $title, $link){

        $fileName = $artist.'-'.$title;
        $fileExtention = '.mp4';

        $yt = new \YouTubeDownloader();

        $links = $yt->getDownloadLinks('https://www.youtube.com/watch?v='.$link);

        return $links;


        set_time_limit(0);
        $file = file_get_contents($links['18']['url']);
        Storage::disk('public')->put($fileName.$fileExtention, $file);

        $cwdPath = getcwd();

        $filePath = $cwdPath.'\\storage\\'.$fileName.$fileExtention;

        $cmd = 'ffmpeg -i '.$filePath.' -f mp3 -ab 128000 -vn '.$fileName.'.mp3';

        shell_exec($cmd);

        return $filePath;
    }
}
