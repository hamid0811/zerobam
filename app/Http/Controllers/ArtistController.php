<?php

namespace App\Http\Controllers;

use App\Album;
use App\Artist;
use App\Http\Resources\Artist as ArtistResource;
use ColorThief\ColorThief;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Spatie\MediaLibrary\Models\Media;


class ArtistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->user()->isArtist()){
            $artists = Artist::where('user_id', auth()->id())->get();
            return view('admins.artists.index', compact('artists'));
        }else{
            $artists = Artist::all();
            foreach ($artists as $key => $artist) {
                $artists[$key]['photo'] = $artist->getFirstMediaUrl('photo', 'medium');
                unset($artists[$key]['media']);
            }
            return view('admins.artists.index', compact('artists'));
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admins.artists.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $artist = new Artist;
        ///////////////////////////////////////////////////////////////
        ////////////////Extract Color From Image///////////////////////
        $img = file_get_contents($request->photo);
        $palette = ColorThief::getPalette($img, 8);
        /////////////////////////////////////////////////////////////////

        $artist->addMedia($request->photo)->withCustomProperties(['colors' => $palette])->toMediaCollection('photo');

        $artist->name = $request->name;
        $imageName = uniqid('img_');
        $artist->photo = $imageName;
        $artist->save();

        $artist['full_url'] = $artist->getFirstMediaUrl('photo', 'medium');

        return view('admins.artists.show');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Artist  $artist
     * @return \Illuminate\Http\Response
     */
    public function show(Artist $artist, $id)
    {
        $user = Auth::user();
        $artist = $artist->where('id', $id)->with('albums')->first();

        if($user->can('view', $artist)){
            $totalPlays = $artist->tracks()->sum('play_count');
            return view('admins.artists.show', compact('artist', 'totalPlays'));
        }else{
            return 'not authorized';
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Artist  $artist
     * @return \Illuminate\Http\Response
     */
    public function edit(Artist $artist, $id)
    {
        $artist = $artist->find($id);
        $user = Auth::user();

        if($user->can('edit', $artist)){
            $media = $artist->getMedia();
            return view('admins.artists.edit', compact('artist', 'media'));
        }else{
            return "not authorized";
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Artist  $artist
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Artist $artist, $id)
    {
        $artist = $artist->find($id);
        $artist->name = $request->name;
        if($request->hasFile('photo')){
            $media = $artist->getFirstMedia('photo');
            if($media != null){
             $media->delete();
            }
            $artist->addMedia($request->photo)->toMediaCollection('photo');
        }
        $artist->save();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Artist  $artist
     * @return \Illuminate\Http\Response
     */
    public function destroy(Artist $artist, Request $request)
    {
        $id = $request->artist_id;
        $user = Auth::user();
        $artist = $artist->find($id);

        if($user->can('delete', $artist)){
            $artist = $artist->find($id);
            $albums = $artist->albums()->get();
            foreach ($albums as $album) {
                foreach ($album->tracks as $track) {
                    $track->delete();
                }
                $album->tracks()->detach();
                $album->delete();
            }
            $artist->delete();

            return view('admins.index');
        }else{
            return 'Not authorized';
        }
    }

    /**
     * Search for an artist.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request, $query)
    {
        $artists = Artist::where('name', 'like',$query.'%')->where('is_published', '1')->get();
        foreach ($artists as $key => $artist) {
            $media = $artist->getFirstMedia('photo');
            $artists[$key]->photo_url = $media->getFullUrl('medium');
        }

        return response()->json($artists);

        if ($request->ajax()) {
            return response()->json($artists)->header('Vary', 'Accept');
        }else{
            return view('admins.artists.index', compact('artists'));
        }
    }

    public function publish(Request $request, Artist $artist){
        $user = Auth::user();
        $artist = Artist::where('id', $request->artist_id)->first();

        if($user->can('update', $artist)){
            if($request->action == "publish" && $artist->is_published == '0'){
                $artist->is_published = 1;
                $artist->save();
                return redirect()->back();
            }else{
                $artist->is_published = 0;
                $artist->save();
                return redirect()->back();
            }
        }else{
            return "No authorized";
        }
    }
}
