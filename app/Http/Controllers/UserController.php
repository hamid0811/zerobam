<?php

namespace App\Http\Controllers;

use App\Artist;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        return view('users.index');
    }

    public function show(Artist $artist, $id)
    {
        $artist = $artist->where(['user_id'=> auth()->id(), 'id' => $id])->with('albums')->first();
        return view ('users.artists.show', compact('artist'));
    }

    public function artists(Artist $artist)
    {
        $artists = $artist->where('user_id', auth()->id())->get();
        return view('users.artists.index', compact('artists'));
    }

    public function edit(Artist $artist, $id)
    {
        $artist = $artist->find($id);
        return view('users.artists.edit', compact('artist'));
    }
}
