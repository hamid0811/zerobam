<?php

namespace App\Http\Controllers\uploads;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class Uploads extends Controller
{
    public function upload (Request $request)
    {
        $artistID = $request->get('artistID');
        $albumID = $request->get('albumID');

        if($files=$request->file('files')){
            foreach($files as $file){
                $name= $file->getClientOriginalName();

                $path = Storage::putFileAs('public/uploads', $file, $name);
                AttachmentController::store($artistID, $albumID);

            }
        }
        return ($request);
    }
}
