<?php

namespace App\Http\Controllers\api;

use App\Album;
use App\Http\Controllers\Controller;
use App\SavedTrack;
use App\Track;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SavedTrackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $localSavedSongs = $request->all();
        $unsyncedTracks = SavedTrack::whereNotIn('track_id', $localSavedSongs)->where('user_id', Auth::id())->with('track')->get()->pluck('track');

        $savedTracks = SavedTrack::where('user_id', Auth::id())->with('track')->get()->pluck('track.id')->toArray();
        $tracks = array_diff($localSavedSongs,$savedTracks);
        if ($tracks) {
            foreach ($tracks as $key => $track) {
                $savedTrack = new SavedTrack;
                $savedTrack->user_id = Auth::id();
                $savedTrack->track_id = $track;
                $savedTrack->save();
            }
        }

        $collection = array();
        foreach ($unsyncedTracks as $key => $savedTrack) {
            $album = Track::where('id', $savedTrack->id)->with('album')->get()->pluck('album');
            $albumMedia = $album[0][0]->getFirstMedia('cover');
            $media = $savedTrack->getFirstMedia('track');
            $collection[] = array(
                'colors' => $albumMedia->custom_properties['colors'],
                'id' => $savedTrack->id,
                'title' => $savedTrack->title,
                'artist' => $savedTrack->artist->name,
                'artist_id' => $savedTrack->artist->id,
                'cover' => $albumMedia->getFullUrl('medium'),
                'url' => $media->getFullUrl(),
                'lyrics' => []
            );
        };

        return $collection;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $savedTrack = new SavedTrack;
        $savedTrack->user_id = Auth::id();
        $savedTrack->track_id = $request->track_id;
        $savedTrack->save();

        return 'success';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $savedTrack = SavedTrack::where(['user_id' => Auth::id(), 'track_id'=> $id])->get();
        // return $savedTrack;
        if($savedTrack->isEmpty()){
            return 'false';
        }else{
            return 'true';
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $savedTrack = SavedTrack::where(['user_id' => Auth::id(), 'track_id'=> $id])->delete();
        return $savedTrack;
    }
}
