<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Playlist;
use App\Track;
use App\TrackPlaylist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PlaylistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $playlists = Playlist::where([
                        ['user_id', '=', auth()->id()],
                        ['name', '!=', 'Saved Songs']
                    ])->get();
        $saved_songs = Playlist::where([
                        ['user_id', '=', auth()->id()],
                        ['name', '=', 'Saved Songs']
                    ])->first();
        return response()->json(['playlists' => $playlists, 'saved_songs' => $saved_songs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $playlistName = $request->name;
        $playlist = new Playlist;
        $playlist->name = $playlistName;
        $playlist->user_id = auth()->id();
        $playlist->save();

        return "Playlist has been created";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $playlist = Playlist::where('id', $id)->first();

        $track_ids = TrackPlaylist::where('playlist_id', $id)->get()->pluck('track_id');

        $tracks = array();

        foreach ($track_ids as $key => $track_id) {
            $track = Track::where('id', $track_id)->with('album')->first();
            $track_media = $track->getFirstMedia('track');

            $album = Track::where('id', $track_id)->with('album')->get()->pluck('album');
            $album_media = $album[0][0]->getFirstMedia('cover');

            $tracks[] = array(
                'colors' => $album_media->custom_properties['colors'],
                'id' => $track->id,
                'title' => $track->title,
                'artist' => $track->artist->name,
                'artist_id' => $track->artist->id,
                'cover' => $album_media->getFullUrl('medium'),
                'url' => $track_media->getFullUrl(),
                'lyrics' => []
            );
        }


        return response()->json(['playlist'=> $playlist, 'tracks'=> $tracks]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function sync(Request $request){
        $user = Auth::id();
        $lists =  $request->all();

        foreach ($lists as $list) {
            $playlist = new Playlist;
            $playlist->name = $list['name'];
            $playlist->user_id = $user;
            $playlist->save();
        }

        return response()->json(['status' => 'success']);
    }

    public function add_track_to_playlist(Request $request){
        $trackPlaylist = new TrackPlaylist;
        $trackPlaylist->track_id = $request->track['id'];
        $trackPlaylist->playlist_id = $request->playlist['id'];
        $trackPlaylist->save();

        return "added to the playlist";
    }
}
