<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');


        if(Auth::attempt($credentials))
        {
            $request->request->add([
                'grant_type'    => 'password',
                'client_id'     => env('OAUTH_CLIENT_ID'),
                'client_secret' => env('OAUTH_CLIENT_SECRET'),
                'username' => $request->email,
                'password' => $request->password,
                'scope' => '*'
            ]);

            // forward the request to the oauth token request endpoint
            $tokenRequest = Request::create('/oauth/token','post');
            return Route::dispatch($tokenRequest);
        }else
        {
            return response('user does not exist');
        }
    }
}
