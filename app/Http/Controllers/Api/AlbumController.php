<?php

namespace App\Http\Controllers\api;

use App\Album;
use App\Http\Controllers\Controller;
use App\Http\Resources\Album as AlbumResource;
use Illuminate\Http\Request;
use League\ColorExtractor\Color;
use League\ColorExtractor\ColorExtractor;
use League\ColorExtractor\Palette;

class AlbumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Album $album, $id)
    {
        $album = Album::where('id', $id)->with(['tracks', 'artist'])->first();

        if($album->artist->is_published){
            $albumMedia = $album->getFirstMedia('cover');
            $album->cover_url = $albumMedia->getFullUrl('large');
            if (isset($albumMedia->custom_properties['colors'])) {
                $album->colors = $albumMedia->custom_properties['colors'];
            }
            foreach ($album->tracks as $key => $track) {
                $media = $track->getFirstMedia('track');
                $album->tracks[$key]->media = $track->getMedia();
                $album->tracks[$key]->media_url = $media->getFullUrl();
            }
            // return $album;
            return new AlbumResource($album);
        }else{
            return 'The artist of this album has been unpublished by the admin or the artist';
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
