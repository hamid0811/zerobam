<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Lyric;
use App\Track;
use App\Trending;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TrackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $lines = $request->json()->all();

        $lyric = new Lyric;
        $lyric->track_id = $id;
        $lyric->user_id = Auth::id();
        $lyric->synced_lyric = json_encode($lines);
        $lyric->lyric = 'test';
        $lyric->save();

        return "lyric has been saved";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $track = Track::where('id', $id)->with('lyrics')->get();
        return $track;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $track = Track::find($id);
        $track->increment('play_count');
        $track->save();

        $date = new Carbon;
        $now = Carbon::now();
        $date->subWeek();
        $song = DB::table('trendings')->where(['track_id' => $id])->get();

        if(!$song->isEmpty()){
            $trendingTrack = Trending::where('track_id', $id)->first();
            $trendingTrack->increment('play_count');
            $trendingTrack->save();
        }else{

            DB::insert('insert into trendings (track_id, play_count, created_at) values (?, 1, ?)', [$id, $now]);
        }

        return 'Play count has been incremented';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
