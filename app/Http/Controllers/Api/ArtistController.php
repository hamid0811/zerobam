<?php

namespace App\Http\Controllers\api;

use App\Artist;
use App\Http\Controllers\Controller;
use App\Http\Resources\Artist as ArtistResource;
use Illuminate\Http\Request;

class ArtistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Artist $artist,$id)
    {
        $artist = $artist->find($id);
        $artist = $artist->where(['id' => $id, 'is_published' => '1'])->with(['albums'])->first();
        $media =  $artist->getFirstMedia('photo');
        if ($media != null) {
            if (isset($media->custom_properties['colors'])) {
                $artist['colors'] = $media->custom_properties['colors'];
            }
            $artist->photo = $artist->getFirstMediaUrl('photo', 'medium');
        }

        //GET THE POPULAR TRACKS
        $artist->popularTracks = $artist->tracks()->with('album', 'lyrics')->orderBy('play_count', 'desc')->limit(5)->get();
        foreach ($artist->popularTracks as $key => $track) {
            $artist->popularTracks[$key]->link = $track->getFirstMediaUrl('track');
            $album = $track->album()->first();
            $albumMedia = $album->getFirstMediaUrl('cover', 'medium');
            if ($albumMedia != null) {
                $artist->popularTracks[$key]->cover = $albumMedia;
            }
        }

        foreach ($artist->albums as $key => $album) {
            $artist->albums[$key]->cover = $album->getFirstMediaUrl('', 'medium');
        }
        return new ArtistResource($artist);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
