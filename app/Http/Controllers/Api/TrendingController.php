<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Track;
use App\Trending;
use Illuminate\Http\Request;

class TrendingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $trending = array();

        $trendingTrackings = Trending::orderBy('play_count', 'asc')->take(15)->get();
        foreach ($trendingTrackings as $key => $track) {
            $track = Track::where('id', $track->track_id)->with('album')->first();
            $media = $track->getFirstMedia('track');
            $cover = $track->album[0]->getFirstMedia('cover');
            $trending[$key]['cover'] = $cover->getFullUrl('medium');
            $trending[$key]['full_url'] = $media->getFullUrl();
            $trending[$key]['title'] = $track->title;
            $trending[$key]['id'] = $track->id;
            $trending[$key]['album_id'] = $track->album[0]->id;
            $trending[$key]['artist_id'] = $track->artist_id;
        }
        return $trending;
    }
}
