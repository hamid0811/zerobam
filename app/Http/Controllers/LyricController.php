<?php

namespace App\Http\Controllers;

use App\Lyric;
use App\Track;
use Illuminate\Http\Request;

class LyricController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $track = Track::find($id);
        return view ('admins.lyrics.create', compact('track'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $lyric = new Lyric;
        $lyric->user_id = auth()->id();
        $lyric->lyric = $request->lyric;
        $lyric->track_id = $request->track_id;
        $lyric->save();

        return redirect('/lyric/'.$lyric->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Lyric  $lyric
     * @return \Illuminate\Http\Response
     */
    public function show(Lyric $lyric, $id)
    {
        $lyric = $lyric->find($id);
        return view('admins.lyrics.show', compact('lyric'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Lyric  $lyric
     * @return \Illuminate\Http\Response
     */
    public function edit(Lyric $lyric, Track $track, $id)
    {
        $lyric = $lyric->find($id);
        $track = $track->where('id', $lyric->track_id)->with(['album', 'artist', 'lyrics'])->first();
        $track->fullUrl = $track->getFirstMediaUrl('track');

        return view('admins.lyrics.edit', compact('lyric', 'track'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Lyric  $lyric
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Lyric $lyric, $id)
    {
        $lyric = $lyric->find($id);
        $lyric->lyric = $request->lyric;
        $lyric->save();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Lyric  $lyric
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lyric $lyric, $id)
    {
        $lyric = $lyric->find($id);
        $lyric->delete();
        return redirect()->back();
    }

    public function updateSyncedLyric(Request $request, Lyric $lyric, $id)
    {
        // dd($request);
        $lines = $request->json()->all();
        // return $lines;

        $lyric = $lyric->find($id);
        $lyric->user_id = auth()->id();
        $lyric->synced_lyric = json_encode($lines);
        $lyric->save();

        return "lyric has been synced";
    }
}
