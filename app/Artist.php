<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Artist extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $fillable = [
        'is_published',
    ];


    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('large')
            ->crop(Manipulations::CROP_CENTER, 640, 640);

        $this->addMediaConversion('medium')
            ->crop(Manipulations::CROP_CENTER, 300, 300);

        $this->addMediaConversion('small')
            ->crop(Manipulations::CROP_CENTER, 64, 64);
    }

    public function albums()
    {
        return $this->hasMany('App\Album');
    }

    public function tracks(){
        return $this->hasMany('App\Track');
    }
}
