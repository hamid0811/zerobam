<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Track extends Model implements HasMedia
{
    use HasMediaTrait;

    public function album()
    {
        return $this->belongsToMany('App\Album', 'track_album');
    }

    public function lyricSyncs()
    {
        return $this->hasMany('App\LyricSyncs');
    }

    public function lyrics(){
        return $this->hasMany('App\Lyric');
    }

    public function artist(){
        return $this->belongsTo('App\Artist');
    }
}
