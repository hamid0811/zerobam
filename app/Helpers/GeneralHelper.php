<?php
namespace App\Helpers;

class GeneralHelper
{
    public static function rgbToHsl( $r, $g, $b ) {
        $oldR = $r;
        $oldG = $g;
        $oldB = $b;
        $r /= 255;
        $g /= 255;
        $b /= 255;
        $max = max( $r, $g, $b );
        $min = min( $r, $g, $b );
        $h;
        $s;
        $l = ( $max + $min ) / 2;
        $d = $max - $min;
            if( $d == 0 ){
                $h = $s = 0; // achromatic
            } else {
                $s = $d / ( 1 - abs( 2 * $l - 1 ) );
            switch( $max ){
                    case $r:
                        $h = 60 * fmod( ( ( $g - $b ) / $d ), 6 );
                            if ($b > $g) {
                            $h += 360;
                        }
                        break;
                    case $g:
                        $h = 60 * ( ( $b - $r ) / $d + 2 );
                        break;
                    case $b:
                        $h = 60 * ( ( $r - $g ) / $d + 4 );
                        break;
                }
        }
        $h = substr(round($h*360), 0, 2);
        $toInteger = (int) $h;
        return array(  $toInteger, round( $s*100), round( $l*100) );
    }

    public static function fromHexToHsl($hex) {
        $hex = array($hex[0].$hex[1], $hex[2].$hex[3], $hex[4].$hex[5]);
        $rgb = array_map(function($part) {
            return hexdec($part) / 255;
        }, $hex);

        $max = max($rgb);
        $min = min($rgb);

        $l = ($max + $min) / 2;

        if ($max == $min) {
            $h = $s = 0;
        } else {
            $diff = $max - $min;
            $s = $l > 0.5 ? $diff / (2 - $max - $min) : $diff / ($max + $min);

            switch($max) {
                case $rgb[0]:
                    $h = ($rgb[1] - $rgb[2]) / $diff + ($rgb[1] < $rgb[2] ? 6 : 0);
                    break;
                case $rgb[1]:
                    $h = ($rgb[2] - $rgb[0]) / $diff + 2;
                    break;
                case $rgb[2]:
                    $h = ($rgb[0] - $rgb[1]) / $diff + 4;
                    break;
            }

            $h /= 6;
        }

        return array( round( $h*360), round( $s*100), round( $l*100) );
    }

    public static function getSongInfo($file)
    {
        $getID3 = new \getID3;


        $ThisFileInfo = $getID3->analyze($file);

        $title =  $ThisFileInfo['tags']['id3v2']['title'][0];
        $artist =  $ThisFileInfo['tags']['id3v2']['artist'][0];
        $album =  $ThisFileInfo['tags']['id3v2']['album'][0];
        $trackNumber =  $ThisFileInfo['tags']['id3v2']['track_number'][0];

        // $Image='data:'.$ThisFileInfo['comments']['picture'][0]['image_mime'].';charset=utf-8;base64,'.base64_encode($ThisFileInfo['comments']['picture'][0]['data']);

        $songsData = array();
        $songsData['title'] = ucwords($title);
        $songsData['artist'] = ucwords($artist);
        $songsData['album'] = ucwords($album);
        $songsData['track_number'] = $trackNumber;

        return $songsData;
    }

}
