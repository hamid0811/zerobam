<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ArtistTableSeeder::class);
        $this->call(AlbumTableSeeder::class);
        $this->call(SingleTableSeeder::class);
        $this->call(TrackTableSeeder::class);
        $this->call(TrackAlbumTableSeeder::class);
        $this->call(SingleTrackTableSeeder::class);
    }
}
