<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLyricsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lyrics', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('track_id')->nullable();
            $table->string('title_english')->nullable();
            $table->string('artist_english')->nullable();
            $table->string('title_dari')->nullable();
            $table->string('artist_dari')->nullable();
            $table->text('lyric');
            $table->json('synced_lyric')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lyrics');
    }
}
