@extends('users.layouts.app')

@section('title')
    Artists
@endsection

@section('alert')
    @if (session('status'))
        <div class="row">
            <div class="col-md-12">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
            </div>
        </div>
    @endif
@endsection

@section('breadcrumb')
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item">Artists</li>
      </ol>
    </nav>
@endsection

@section('sidebar')
    <form method="get" action="{{ url('artist/search') }}">
        {{csrf_field()}}
        @method('get')
        <div class="input-group mb-3">
          <input type="text" class="form-control" placeholder="Search..." name="query">
          <div class="input-group-append">
            <button type="submit" class="btn btn-outline-secondary" type="button">Seach</button>
          </div>
        </div>
    </form>

    <div class="card">
        <ul class="list-group list-group-flush">
            <a href="/artist/create" class="btn btn-primary">New Artist</a>
            <!-- <a href="#" class="btn btn-danger">Edit artist</a> -->
        </ul>
    </div>
@endsection

@section('content')
    <div class="row">
        @foreach($artists as $artist)
        <div class="col-md-4">
            <div class="card" style="margin-bottom: 15px;">
                <div class="card-header">
                    {{ $artist->name }}
                </div>
                <div class="card-body">
                    <a href="/user/artist/{{ $artist->id }}">
                        <img class="img-fluid" src="{{ $artist->getFirstMediaUrl('photo', 'medium')}}">
                    </a>
                </div>
            </div>
        </div>
        @endforeach
    </div>
@endsection
