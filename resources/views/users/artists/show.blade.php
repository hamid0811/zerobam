@extends('users.layouts.app')

@section('title')
{{$artist->name}}
@endsection

@section('sidebar')
<div class="card">
    <img class="card-img-top" src="{{$artist->getFirstMediaUrl('photo', 'medium')}}" alt="Card image cap">
    <div class="card-body">
        <h5 class="card-title">{{$artist->name}}</h5>
    </div>
    <ul class="list-group list-group-flush">
        <a href="#" class="list-group-item">Overview</a>
        <a href="#" class="list-group-item">Albums</a>
        <a href="#" class="list-group-item">Singles</a>
        <a href="#" class="list-group-item">Bio</a>
        <a href="/user/artist/edit/{{ $artist->id }}" class="list-group-item">Edit artist</a>
    </ul>
</div>
@endsection

@section('alert')
    @if (session('status'))
        <div class="col-md-12">
            @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
            @endif
        </div>
    @endif
@endsection

@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/user/artist">Artists</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{$artist->name}}</li>
  </ol>
</nav>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="float-left">
                        <h4>Albums</h4>
                    </div>
                    <div class="float-right">
                        <a href="/album/create/{{ $artist->id }}" class="btn btn-primary">+</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        @foreach($artist->albums as $album)
            @if($album->type == "album")
                <div class="col-md-3">
                    <div class="card" style="margin-bottom: 15px">
                        <div class="card-header">
                            {{ $album->name }}
                        </div>
                        <div class="card-body">
                            <a href="/album/{{ $album->id }}">
                                <img class="img-fluid" src="{{ $album->getFirstMediaUrl('', 'medium')}}">
                            </a>
                        </div>
                    </div>
                </div>
            @endif
        @endforeach
    </div>

    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="float-left">
                        <h4>Singles</h4>
                    </div>
                    <div class="float-right">
                        <a href="" class="btn btn-primary">+</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <br>
    <div class="row">
        @foreach($artist->albums as $album)
            @if($album->type == "single")
                <div class="col-md-3">
                    <div class="card" style="margin-bottom: 15px">
                        <div class="card-header">
                            {{ $album->name }}
                        </div>
                        <div class="card-body">
                            <a href="/album/{{ $album->id }}">
                                <img class="img-fluid" src="{{ $album->getFirstMediaUrl('', 'medium')}}">
                            </a>
                        </div>
                    </div>
                </div>
            @endif
        @endforeach
    </div>
@endsection
