@extends('admins.layouts.app')

@section('title')
    Edit Artist - Afghanjam.com
@endsection

@section('alert')
    @if (session('status'))
        <div class="row">
            <div class="col-md-12">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
            </div>
        </div>
    @endif
@endsection

@section('breadcrumb')
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/artist">Artists</a></li>
        <li class="breadcrumb-item"><a href="/artist/{{$artist->id}}">{{$artist->name}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">Edit</li>
      </ol>
    </nav>
@endsection

@section('sidebar')
    <div class="card">
        <img class="card-img-top" src="{{$artist->getFirstMediaUrl('photo', 'medium')}}" alt="Card image cap">
        <div class="card-body">
            <h5 class="card-title">{{$artist->name}}</h5>
        </div>
        <ul class="list-group list-group-flush">
            <a href="#" class="list-group-item">Home</a>
            <a href="#" class="list-group-item">Albums</a>
            <a href="#" class="list-group-item">Singles</a>
            <a href="#" class="list-group-item">Bio</a>
            <a href="/artist/edit/{{ $artist->id }}" class="btn btn-danger">Edit artist</a>
        </ul>
    </div>
@endsection

@section('content')
    <form method="post" action="{{ url('artist/edit',$artist->id) }}" enctype="multipart/form-data">
        {{csrf_field()}}
        @method('patch')
        <div class="form-group">
            <label for="name">Name:</label>
            <input type="text" class="form-control" name="name" value="{{ $artist->name }}">
        </div>
        <div class="form-group">
            <label for="photo">Photo:</label>
            <input type="file" name="photo" class="form-control-file">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

    <hr>

    <div class="row">
        <div class="col-6">
            <form method="post" action="{{ url('artist/publish') }}" enctype="multipart/form-data">
                {{csrf_field()}}
                @method('patch')
                <input type="hidden" name="artist_id" value="{{$artist->id}}">

                @if($artist->is_published != 1)
                    <input type="hidden" name="action" value="publish">
                @else
                    <input type="hidden" name="action" value="unpublish">
                @endif

                @if($artist->is_published != 1)
                    <button type="submit" class="btn btn-primary">Publish Artist</button>
                @else
                    <button type="submit" class="btn btn-danger">Unpublish Artist</button>
                @endif
            </form>
        </div>
        <div class="col-6">
            <div class="float-right">
               <form method="post" action="{{ url('artist/delete') }}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    @method('delete')
                    <input type="hidden" name="artist_id" value="{{$artist->id}}">
                    <button type="submit" class="btn btn-danger">Delete Artist</button>
                </form>
            </div>

        </div>
    </div>


@endsection
