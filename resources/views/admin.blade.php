<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#212529">
    <link rel="stylesheet" type="text/css" href="/css/admin/admin.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="manifest" href="{{ asset('/manifest.json') }}" />
</head>
<body>
    <style type="text/css">
        body{
            font-family: 'Muli', sans-serif;
        }
    </style>

    <div id="app">
        <App ></App>
    </div>

    <script src="/js/admin/main.js" charset="UTF-8"></script>
</body>
</html>
