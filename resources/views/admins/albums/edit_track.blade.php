@extends('admins.layouts.app')

@section('title')
    Edit Track: {{$track->name}}
@endsection

@section('breadcrumb')
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/artist">Artists</a></li>
        <li class="breadcrumb-item"><a href="/artist/{{$artist->id}}">{{$artist->name}}</a></li>
        <li class="breadcrumb-item"><a href="/artist/albums">Albums</a></li>
        <li class="breadcrumb-item"><a href="/album/{{$track->album[0]->id}}">{{$track->album[0]->name}}</a></li>
        <li class="breadcrumb-item"><a href="/album/{{$track->id}}">{{$track->title}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">Edit</li>
      </ol>
    </nav>
@endsection

@section('sidebar')
    <img style="padding-bottom: 15px;" class="img-fluid" src="{{$track->album[0]->getFirstMediaUrl('cover', 'medium')}}">
    <h4>{{$track->name}}</h4>
    <form action="/album/delete" method="POST">
        @method('DELETE')
        @csrf
        <input type="hidden" name="album_id" value="{{$track->album[0]->id}}">
        <button type="submit" class="btn btn-danger">Delete</button>
    </form>

@endsection

@section('content')
    <div id="app">
        <sync-lyrics-component :track="{{json_encode($track)}}"></sync-lyrics-component>
    </div>
    <form method="post" action="{{ url('track/edit',$track->id) }}" enctype="multipart/form-data">
        {{csrf_field()}}
        @method('patch')
        <div class="form-group">
            <label for="lyrics">Lyrics</label>
            <textarea class="form-control" id="textarea" dir="rtl" style="text-align: center; font-size: 1.2em;" name="lyrics" rows="20">{{$track->lyrics}}</textarea>
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    <button id="store"  onclick="store(); return false;">Store</button>
@endsection
<script>
    function store() {
      var txt = {!! json_encode($track->lyrics) !!};

      var txttostore = '<p>' + txt.replace(/\n/g, "</p>\n<p>") + '</p>';
      document.getElementById("sync").innerHTML = txttostore;

      console.log(txttostore);
    }
</script>
