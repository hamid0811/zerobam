@extends('admins.layouts.app')

@section('title')
    Edit Album: {{$album->artist->name}} - {{$album->name}}
@endsection

@section('breadcrumb')
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/artist">Artists</a></li>
        <li class="breadcrumb-item"><a href="/artist/{{$album->artist->id}}">{{$album->artist->name}}</a></li>
        <li class="breadcrumb-item"><a href="/artist/albums">Albums</a></li>
        <li class="breadcrumb-item"><a href="/album/{{$album->id}}">{{$album->name}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">Edit</li>
      </ol>
    </nav>
@endsection


@section('content')
    <img style="padding-bottom: 15px;" class="img-fluid" src="{{$album->getFirstMediaUrl('cover', 'medium')}}">
    <h4>{{$album->name}}</h4>
    <form action="/album/delete" method="POST">
        @method('DELETE')
        @csrf
        <input type="hidden" name="album_id" value="{{$album->id}}">
        <button type="submit" class="btn btn-danger">Delete</button>
    </form>

    <form method="post" action="{{ url('album/edit',$album->id) }}" enctype="multipart/form-data">
        {{csrf_field()}}
        @method('patch')
        <div class="form-group">
            <label for="type">Type</label>
            <select name="type" class="form-control">
                @if($album->type == "album")
                    <option name="type" selected value="album">Album</option>
                    <option name="type" value="single">Single</option>
                @else
                    <option name="type" value="album">Album</option>
                    <option name="type" selected value="single">Single</option>
                @endif
            </select>
        </div>
        <div class="form-group">
            <label for="name">Name:</label>
            <input type="text" class="form-control" name="name" value="{{ $album->name }}">
        </div>
        <div class="form-group">
                <label for="photo">Cover:</label>
                <input type="file" name="cover" class="form-control-file">
            </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
