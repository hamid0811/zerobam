@extends('admins.layouts.app')

@section('title')
    {{$album->artist->name}} - {{$album->name}}
@endsection

@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/artist">Artists</a></li>
    <li class="breadcrumb-item"><a href="/artist/{{$album->artist->id}}">{{$album->artist->name}}</a></li>
    <li class="breadcrumb-item"><a href="/artist/albums">Albums</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{$album->name}}</li>
  </ol>
</nav>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="card">
                <img class="card-img-top" src="{{ $album->getFirstMediaUrl('', 'medium') }}" alt="Album Image">
                <div class="card-body">
                    <h5 class="card-title">{{$album->name}}</h5>
                </div>
                <ul class="list-group list-group-flush">
                    <a href="/album/edit/{{$album->id}}" class="btn btn-primary">Edit album</a>
                </ul>
            </div>
        </div>
        <div class="col-md-9">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Play Count</th>
                        <th></th>
                        <th>Lyrics</th>
                        <th>Delete</th>
                        <th>Edit</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($album->tracks as $track)
                    <tr>
                        <th>{{ $track->track_number }}</th>
                        <th>{{ $track->title }}</th>
                        <th>{{ $track->play_count }}</th>
                        <th>
                            <audio controls>
                                <source src="{{$track->getFirstMediaUrl('track')}}" type="audio/mpeg">
                                Your browser does not support the audio element.
                            </audio>
                        </th>
                        <th>
                            <span>0</span>
                        </th>
                        <th>
                            <form action="/album/track/delete" method="POST">
                                @method('DELETE')
                                @csrf
                                <input type="hidden" name="album_id" value="{{$album->id}}">
                                <input type="hidden" name="track_id" value="{{$track->id}}">
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        </th>
                        <th>
                            <a class="btn btn-primary" href="/track/edit/{{$track->id}}" class="btn btn-Success">Edit</a>
                        </th>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <add-album-tracks :album="{{$album}}"></add-album-tracks>
        </div>
    </div>
</div>
@endsection
