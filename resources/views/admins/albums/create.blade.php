@extends('admins.layouts.app')

@section('title')
    Add neew album
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Add new album
                </div>
                <div class="card-body">
                    <form method="post" action="{{ url('album/store') }}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        @method('post')
                        <div class="form-group">
                            <label for="type">Type</label>
                            <select name="type" class="form-control">
                                <option name="type" value="album">Album</option>
                                <option name="type" value="single">Single</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="name">Name:</label>
                            <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                        </div>
                        <div class="form-group">
                            <label for="release_date">Release Date:</label>
                            <input type="date" class="form-control" name="release_date">
                        </div>
                        <div class="form-group">
                            <label for="cover">Photo:</label>
                            <input type="file" name="photo" class="form-control-file">
                        </div>
                        <input type="hidden" value="{{ $artist->id }}" name="artist_id">

                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
