@extends('admins.layouts.app')

@section('content')
    <form method="post" action="{{ url('lyric/create') }}" enctype="multipart/form-data">
        {{csrf_field()}}
        @method('put')
        <div class="form-group">
            <input type="hidden" name="track_id" value="{{$track->id}}">
            <label for="lyrics">Lyric</label>
            <textarea class="form-control" id="textarea" dir="rtl" style="text-align: center; font-size: 1.2em;" name="lyric" rows="20"></textarea>
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
