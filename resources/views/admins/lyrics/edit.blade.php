@extends('admins.layouts.app')

@section('content')
    <div id="app">
        <sync-lyrics-component :track="{{json_encode($track)}}" :lyric="{{json_encode($lyric)}}"></sync-lyrics-component>
    </div>
    <form method="post" action="{{ url('lyric',$lyric->id) }}" enctype="multipart/form-data">
        {{csrf_field()}}
        @method('patch')
        <div class="form-group">
            <label for="lyrics">Lyric</label>
            <textarea class="form-control" id="textarea" dir="rtl" style="text-align: center; font-size: 1.2em;" name="lyric" rows="20">{{$lyric->lyric}}</textarea>
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
