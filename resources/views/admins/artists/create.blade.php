@extends('admins.layouts.app')

@section('title')
    Add a new artist
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Edit site settings
                </div>
                <div class="card-body">
                    <form method="post" action="{{ url('artist/store') }}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        @method('post')
                        <div class="form-group">
                            <label for="name">Name:</label>
                            <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                        </div>
                        <div class="form-group">
                                <label for="photo">Photo:</label>
                                <input type="file" name="photo" class="form-control-file">
                            </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
