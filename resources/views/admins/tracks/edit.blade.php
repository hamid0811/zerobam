@extends('admins.layouts.app')

@section('title')
    Edit Track: {{ $track->artist->name }} - {{ $track->album[0]->name }} - {{$track->title}}
@endsection

@section('breadcrumb')

@endsection

@section('sidebar')

@endsection

@section('content')
    <form method="post" action="" enctype="multipart/form-data">
        {{csrf_field()}}
        @method('patch')
        <div class="form-group">
            <label for="type">Title</label>
            <input type="text" name="title" class="form-control" value="{{ $track->title }}">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

    <hr>

    <h2>Lyrics</h2>

    <table class="table">
        <tbody>
            @foreach($track->lyrics as $lyric)
                <tr>
                    <td><a href="/lyric/{{$lyric->id}}">{{ $lyric->id }}</a></td>
                    <td>
                        <a class="btn btn-danger float-right" href="/lyric/delete/{{$lyric->id}}">Delete</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <a href="/lyric/create/{{$track->id}}" class="btn btn-primary">New Lyric</a>
@endsection
