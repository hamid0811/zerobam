import NavBar from '../../components/nav-bar/nav-bar.vue';

export default {
    name: 'PlaylistPage',
    props:[
        'id',
    ],
    components:{
        'nav-bar': NavBar
    },
    data(){
        return {
            tracks: {},
            playlist: {}
        }
    },
    mounted(){
        this.fetchPlaylist(this.id);
        Event.$on('reloadPlaylist', (playlist) => {
            this.fetchPlaylist(playlist.id);
            console.log(playlist);
        });
    },
    methods: {
        play(tracks, index){
            Event.$emit('playTrack', tracks, index);
        },
        fetchPlaylist(id){
            let database = indexedDB.open("Zerobam", 1);
            database.onsuccess = ()=>{
                var self = this;
                var db = database.result;
                var tx = db.transaction("playlists", 'readonly');
                var objectStore = tx.objectStore('playlists');
                var request = objectStore.get(this.id);

                request.onerror = function(event) {
                    console.log('Transaction failed');
                };

                request.onsuccess = function( event) {
                    if (request.result) {
                        self.playlist = request.result;
                    } else {
                        console.log('No data record');
                    }
                };
            }
            // axios.get('/api/playlist/'+id).then(response => {
            //     this.tracks = response.data.tracks;
            //     this.playlist = response.data.playlist;
            // })
        }
    }
}
