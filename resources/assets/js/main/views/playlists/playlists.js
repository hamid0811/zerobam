import NewPlaylist from './components/newPlaylistComponent.vue';
import NavBar from '../../components/nav-bar/nav-bar.vue';

export default {
    name: 'PlaylistsPage',
    components:{
        'new-playlist': NewPlaylist,
        'nav-bar': NavBar,
    },
    data(){
        return {
            playlists: {},
            saved_songs: {}
        }
    },
    mounted(){
        Event.$on('reloadPlaylists', () => {
            this.fetchAllPlaylists();
        });
        this.fetchAllPlaylists();
    },
    methods: {
        fetchAllPlaylists(){
            let database = indexedDB.open("Zerobam", 1);

            // database.onupgradeneeded = function(event) {
            //   var db = event.target.result;

            //   var objectStore = db.createObjectStore("playlists", { keyPath: "local_id" });

            //   objectStore.createIndex("local_id", "local_id", { unique: false });
            //   objectStore.createIndex("name", "name", { unique: false });
            //   // objectStore.createIndex("created_at", "created_at", { unique: false });
            //   // objectStore.createIndex("updated_at", "updated_at", { unique: false });
            // };


            database.onsuccess = ()=>{
                var self = this;
                var db = database.result;
                var tx = db.transaction("playlists", 'readonly');
                var objectStore = tx.objectStore('playlists');

                var request = objectStore.getAll();
                request.onerror = function(event) {
                    console.log('Transaction failed');
                };

                request.onsuccess = function( event) {
                    if (request.result) {
                        self.playlists = request.result;
                        console.log(request.result);
                    } else {
                        console.log('No data record');
                    }
                };
            }
            // axios.get('/api/playlists').then(response => {
            //     this.playlists = response.data.playlists;
            //     this.saved_songs = response.data.saved_songs;
            //     // this.persistToDatabase();
            // })
        },
        persistToDatabase(){
            let indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB
            var DBOpenRequest = window.indexedDB.open("Zerobam", 1);
            var db;

            DBOpenRequest.onsuccess = function(event) {
              // store the result of opening the database in db.
              db = DBOpenRequest.result;
            };

            DBOpenRequest.onupgradeneeded = function(event) {
              var db = event.target.result;

              var objectStore = db.createObjectStore("playlists", { keyPath: "id" });

              // define what data items the objectStore will contain

              objectStore.createIndex("name", "name", { unique: false });
              objectStore.createIndex("created_at", "created_at", { unique: false });
              objectStore.createIndex("updated_at", "updated_at", { unique: false });
            };

            let open = indexedDB.open("Zerobam", 1);
            open.onsuccess = ()=> {
                let db = open.result;
                var tx = db.transaction("playlists", 'readwrite')
                // create an object store on the transaction
                var objectStore = tx.objectStore("playlists");
                // make a request to add our newItem object to the object store

                this.playlists.forEach((playlist, key)=>{
                    var objectStoreRequest = objectStore.add(playlist);
                })
            }
        }
    }
}
