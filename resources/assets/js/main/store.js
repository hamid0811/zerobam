import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        playingTrack:{},
        highlightedLine:{},
        scrollPosition:{},
        isPlayerActive: {},
        trackDuration: '0:00',
        trackCurrentTime: 0,
        currentLyricLine: '',
        trackCurrentTimeInSecs: '',
        trackDurationInSecs:0,
        trackCurrentTimeInMilSecs: 0,
        isPlaying: false,
        shuffle: false,
        playingArtist:{},
        queue: {},
    },
    mutations: {
        setTrackCurrentTime(state, trackCurrentTime) {
          state.trackCurrentTime = trackCurrentTime
        },
        setPlayingTrack(state, playingTrack) {
          state.playingTrack = playingTrack
        },
        setCurrentLyricLine(state, line) {
            state.currentLyricLine = line;
        },
        setTrackCurrentTimeInSecs(state, secs) {
          state.trackCurrentTimeInSecs = secs;
        },
        setTrackDurationInSecs(state, payload) {
            state.trackDurationInSecs = payload
        },setTrackCurrentTimeInMilSecs(state, payload) {
            state.trackCurrentTimeInMilSecs = payload
        },
        setIsPlaying(state, payload) {
          state.isPlaying = payload;
        },
        setShuffle(state, payload) {
          state.shuffle = payload;
        },
        setTrackDuration(state, payload){
            state.trackDuration = payload;
        },
        setPlayingArtist(state, payload){
            state.playingArtist = payload;
        },
        setQueue(state, tracks){
            state.queue = tracks;
        }
    },
    getters:{
        highlightedLine(state){
            return state.highlightedLine;
        },
        scrollPosition(state){
            return state.scrollPosition;
        },
        isPlayerActive(state){
            return state.isPlayerActive;
        },
        trackDuration(state){
            return state.trackDuration;
        },
        trackDuration(state){
            return state.playingTrack;
        },
        trackCurrentTime: state => state.trackCurrentTime,
        playingTrack: state => state.playingTrack,
        currentLyricLine: state => state.currentLyricLine,
    },
    setters:{
        playingTrack: state => state.playingTrack,
    }
});
