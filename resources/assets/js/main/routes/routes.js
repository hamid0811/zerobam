import SearchPage from '.././components/SearchPage.vue';
import Artist from '.././components/artists/Artist.vue';
import AlbumPage from '.././components/albums/AlbumPage.vue';
import HomePage from '.././components/home/HomePage.vue';
import TrackPage from '.././components/tracks/TrackPage.vue';
import AuthPage from '.././components/auth/AuthPage.vue';
import LyricPage from '.././components/lyrics/LyricPage.vue';
import PlayerPage from '.././components/PlayerPage.vue';
import SavedSongs from '.././components/SavedSongs';
import PlaylistRoute from './PlaylistRoute.vue';
import PlaylistsPage from '.././views/playlists/playlists.vue';
import PlaylistPage from '.././views/playlist/playlist.vue';

export default [
    { path: '/', component: HomePage, name: 'HomePage'},
    { path: '/search', component: SearchPage, name: 'SearchPage'},
    { path: '/artist/:id/', component: Artist, props: true, name: 'ArtistPage' },
    { path: '/album/:id/', component: AlbumPage, props: true, name: 'AlbumPage'},
    { path: '/track/:id/', component: TrackPage, props: true},
    { path: '/vuelogin/', component: AuthPage},
    { path: '/lyric/:id', component: AuthPage, props: true},
    { path: '/player', component: PlayerPage, name: 'PlayerPage', props: true},
    { path: '/saved-songs', component: SavedSongs, name: 'SavedSongs'},
    { path: '/playlists', component: PlaylistsPage, name: 'PlaylistsPage'},
    { path: '/playlist/:id', component: PlaylistPage, name: 'PlaylistPage', props: true},
]
