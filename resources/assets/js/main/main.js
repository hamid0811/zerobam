require('./bootstrap');
window.Vue = require('vue');
window.VueRouter = require('vue-router').default;
import 'babel-polyfill';

import App from'./App.vue';
import Routes from './routes/routes';
import BootstrapVue from 'bootstrap-vue';
import { store } from './store';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

import VueKeepScrollPosition from 'vue-keep-scroll-position';
import VModal from 'vue-js-modal';
import ToggleButton from 'vue-js-toggle-button';
import VueLocalForage from 'vue-localforage';
import VueLazyload from 'vue-lazyload'


var VueScrollTo = require('vue-scrollto');

Vue.use(VueLazyload, {
  preLoad: 1.3,
  error: '/images/error.png',
  loading: '/images/loading.gif',
  attempt: 1
})
Vue.use(VueLocalForage);
Vue.use(ToggleButton);
Vue.use(VueScrollTo);
Vue.use(VModal);
Vue.use(VueKeepScrollPosition);
Vue.use(VueRouter);
Vue.use(BootstrapVue);
window.Event = new Vue();

const router = new VueRouter({
    mode:'history',
    base:'/play',
    fallback: true,
    routes: Routes,
    scrollBehavior (to, from, savedPosition) {
        if ('scrollRestoration' in history) { history.scrollRestoration = 'manual'; }
        return new Promise((resolve, reject) => {
            Event.$on('scrollBeforeEnter', ()=>{
                resolve(savedPosition || { x: 0, y: 0 })
            });
        })
    }
});

var eventHub = new Vue();

const app = new Vue({
    el: '#app',
    store,
    router,
    components:{
        'App': App,
    }
});
