
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import BootstrapVue from 'bootstrap-vue'

window.Vue = require('vue');
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('sync-lyrics-component', require('./components/SyncLyricsComponent.vue'));
Vue.component('add-album-tracks', require('./components/albums/AddTracks.vue'));
Vue.component('radiojavan-component', require('./components/RadiojavanComponent.vue'));

const app = new Vue({
    el: '#app'
});
